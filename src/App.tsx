import { Box, ChakraProvider } from "@chakra-ui/react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { Landing, PersonalityTest, QuizResult } from "./pages";
import { Header } from "./components";

function App() {
  return (
    <ChakraProvider>
      <Box h="100vh">
        <Router>
          <Header />
          <Routes>
            <Route path="/" element={<Landing />} />
            <Route path="/personality-test" element={<PersonalityTest />} />
            <Route path="/result" element={<QuizResult />} />
          </Routes>
        </Router>
      </Box>
    </ChakraProvider>
  );
}

export default App;
