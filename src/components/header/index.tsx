import {
  Box,
  Button,
  ButtonGroup,
  Container,
  Flex,
  HStack,
  Link,
  useColorModeValue,
} from "@chakra-ui/react";
import { Link as ReactRouterLink } from "react-router-dom";

function Header() {
  return (
    <Box as="section">
      <Box
        as="nav"
        bg="bg-surface"
        boxShadow={useColorModeValue("sm", "sm-dark")}
      >
        <Container py={4}>
          <HStack justify="space-between" spacing="10">
            <Flex flex="1">
              <ButtonGroup spacing="8" variant="link">
                <Button>
                  <Link as={ReactRouterLink} to="/">
                    Home
                  </Link>
                </Button>
              </ButtonGroup>
            </Flex>
          </HStack>
        </Container>
      </Box>
    </Box>
  );
}

export { Header };
