import { customConfiguration } from "./custom";
import { defaultConfiguration } from "./default";

const finalConfiguration = { ...defaultConfiguration, ...customConfiguration };

export default finalConfiguration;
