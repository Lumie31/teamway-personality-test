import { ListItem, Radio } from "@chakra-ui/react";
import { IOption } from "../../../interfaces/option";

interface IProps {
  isActive: boolean;
  onSelect: (option: IOption) => void;
  option: IOption;
}

function Option(props: IProps) {
  return (
    <ListItem
      border="1px"
      borderColor={props.isActive ? "teal.300" : "gray.300"}
      bgColor="white"
      p={3}
      rounded={4}
      onClick={() => props.onSelect(props.option)}
    >
      <Radio
        fontSize="6xl"
        size="sm"
        name={`${props.option.id}`}
        colorScheme="green"
        value={props.option.id}
      >
        {props.option.value}
      </Radio>
    </ListItem>
  );
}

export { Option };
