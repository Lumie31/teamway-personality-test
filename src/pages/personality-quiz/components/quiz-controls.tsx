import { ArrowBackIcon, ArrowForwardIcon } from "@chakra-ui/icons";
import { Button, Flex } from "@chakra-ui/react";

interface IProps {
  onClickSubmit: () => void;
  onClickPrev: () => void;
  onClickNext: () => void;
  isNextDisabled: boolean;
  isLastPage: boolean;
  canSubmit: boolean;
  page: number;
}

function QuizControls(props: IProps) {
  const isPrevDisabled = props.page < 1;
  return (
    <Flex mt={4} justifyContent="space-between">
      <Button
        leftIcon={<ArrowBackIcon />}
        disabled={isPrevDisabled}
        onClick={props.onClickPrev}
        type="button"
        role="button"
      >
        Prev
      </Button>
      <Button
        rightIcon={<ArrowForwardIcon />}
        hidden={props.isLastPage}
        disabled={props.isNextDisabled}
        onClick={props.onClickNext}
        type="button"
        role="button"
      >
        Next
      </Button>
      <Button
        hidden={!props.isLastPage}
        disabled={!props.canSubmit}
        type="button"
        onClick={props.onClickSubmit}
        colorScheme="teal"
        role="button"
      >
        Submit
      </Button>
    </Flex>
  );
}

export { QuizControls };
