import { List, RadioGroup } from "@chakra-ui/react";
import { IOption } from "../../../interfaces/option";
import { Option } from "./option";

interface IProps {
  answer: number;
  onSelect: (option: IOption) => void;
  options: IOption[];
  questionId: number;
}

function OptionList(props: IProps) {
  return (
    <RadioGroup value={props.answer}>
      <List spacing={2}>
        {props.options.map((option) => {
          const isActive = props.answer === option.id;
          return (
            <Option
              key={`question-${props.questionId}-option-${option.id}`}
              onSelect={props.onSelect}
              isActive={isActive}
              option={option}
            />
          );
        })}
      </List>
    </RadioGroup>
  );
}

export { OptionList };
