import { Box, Center, Text, Link, Button } from "@chakra-ui/react";
import {
  Link as ReactRouterLink,
  Navigate,
  useLocation,
} from "react-router-dom";
import { Verdict } from "../../utils/quiz";

function QuizResult() {
  const { state } = useLocation() as { state: { verdict: Verdict } };

  if (!state || !state.verdict) {
    return <Navigate to="/" />;
  }

  const { verdict } = state;

  return (
    <Center minH={336} w="100vw" h="calc(100% - 50px)">
      <Box textAlign="center">
        <Text id="verdict-text" mb={8} fontSize="3xl" fontWeight="bold">
          You are an {verdict}
        </Text>
        <Link as={ReactRouterLink} to="/" replace>
          <Button colorScheme="teal" size="lg">
            Go home
          </Button>
        </Link>
      </Box>
    </Center>
  );
}

export { QuizResult };
