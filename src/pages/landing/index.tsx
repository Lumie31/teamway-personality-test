import { Box, Button, Center, Link, Text } from "@chakra-ui/react";
import { Link as ReactRouterLink } from "react-router-dom";

function Landing() {
  return (
    <Center minH={336} w="100vw" h="calc(100% - 50px)">
      <Box textAlign="center">
        <Text mb={8} fontSize="3xl" fontWeight="bold">
          Are you an introvert or an extrovert?
        </Text>
        <Link as={ReactRouterLink} to="/personality-test">
          <Button id="take-quiz-button" colorScheme="teal" size="lg">
            Take quiz!
          </Button>
        </Link>
      </Box>
    </Center>
  );
}

export { Landing };
