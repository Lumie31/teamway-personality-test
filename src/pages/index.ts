import { Landing } from "./landing";
import { PersonalityTest } from "./personality-quiz";
import { QuizResult } from "./quiz-result";

export { Landing, PersonalityTest, QuizResult };
