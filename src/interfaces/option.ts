interface IOption {
  id: number;
  value: string;
}

export type { IOption };
