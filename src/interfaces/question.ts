import { IOption } from "./option";

interface IQuestion {
  id: number;
  question: string;
  options: IOption[];
}

export type { IQuestion };
