import Enzyme, { shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { QuizResult } from "../../pages";

Enzyme.configure({ adapter: new Adapter() });

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    state: {
      verdict: "Extrovert",
    },
  }),
}));

describe("QuizResult", () => {
  it("renders the verdict passed via location state", () => {
    const page = shallow(<QuizResult />);
    const verdictTextElement = page.find("#verdict-text");
    expect(verdictTextElement.text()).toEqual("You are an Extrovert");
  });
});
