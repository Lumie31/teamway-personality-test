import { calculateVerdict } from "../../utils";
import { getRandomIndices, Verdict } from "../../utils/quiz";

describe("calculateVerdict", () => {
  it("returns an 'Introvert' verdict for very introverted choices", () => {
    const answers = [1, 1, 1, 1];
    const verdict = calculateVerdict(answers);
    expect(verdict).toStrictEqual(Verdict.Introvert);
  });

  it("returns an 'Introvert' verdict for slightly introverted choices", () => {
    const answers = [2, 2, 2, 3];
    const verdict = calculateVerdict(answers);
    expect(verdict).toStrictEqual(Verdict.Introvert);
  });

  it("returns an 'Extrovert' verdict for very extroverted choices", () => {
    const answers = [4, 4, 4, 4];
    const verdict = calculateVerdict(answers);
    expect(verdict).toStrictEqual(Verdict.Extrovert);
  });

  it("returns an 'Extrovert' verdict for slightly extroverted choices", () => {
    const answers = [2, 3, 3, 3];
    const verdict = calculateVerdict(answers);
    expect(verdict).toStrictEqual(Verdict.Extrovert);
  });

  it("returns an 'Extrovert' verdict when the choices are balanced", () => {
    const answers = [1, 1, 4, 4];
    const verdict = calculateVerdict(answers);
    expect(verdict).toStrictEqual(Verdict.Extrovert);
  });

  it("throws an error when no choices have been selected", () => {
    const answers: number[] = [];
    expect(() => calculateVerdict(answers)).toThrowError();
  });
});

describe("getRandomIndices", () => {
  it("returns an array of the required size within the specified available size", () => {
    const requiredSize = 5;
    const availableSize = 6;
    const indices = getRandomIndices(requiredSize, availableSize);

    expect(indices.length).toStrictEqual(requiredSize);
    const arrayMatchesConstraints = indices.every((x) => x <= availableSize);
    expect(arrayMatchesConstraints).toStrictEqual(true);
  });

  it("throws an error when required size is above the available size", () => {
    expect(() => getRandomIndices(5, 4)).toThrowError();
  });

  it("throws an error when required size is negative to avoid unexpected behavior", () => {
    expect(() => getRandomIndices(-1, 4)).toThrowError();
  });
});
