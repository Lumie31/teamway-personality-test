enum Verdict {
  Extrovert = "Extrovert",
  Introvert = "Introvert",
}

const calculateVerdict = (answers: number[]): Verdict => {
  if (answers.length === 0) {
    throw new Error("Invalid input: verdict cannot be drawn without answers");
  }

  const lowerBound = answers.length;
  const upperBound = lowerBound * 4;
  const totalScore = answers.reduce((total, curr) => total + curr, 0);
  const result = ((totalScore - lowerBound) / (upperBound - lowerBound)) * 100;
  const verdict = result >= 50 ? Verdict.Extrovert : Verdict.Introvert;
  return verdict;
};

const getRandomIndices = (
  requiredSize: number,
  availableSize: number
): number[] => {
  if (requiredSize < 0) {
    throw new Error("Invalid usage: required size should be positive");
  }
  if (requiredSize > availableSize) {
    throw new Error(
      "Invalid usage: required size should not exceed available size"
    );
  }
  const indices = Array.from(Array(availableSize).keys());
  indices.sort(() => Math.random() - 0.5);
  return indices.slice(0, requiredSize);
};

export { calculateVerdict, getRandomIndices, Verdict };
